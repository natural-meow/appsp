'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  // BASE_API: '"https://openappsp.anji-plus.com/sp"', //开发环境
  BASE_API: '"http://127.0.0.1:8081/sp"', //开发环境
})
