//
//  AppspModule.m
//  AJAppspUniPlugin
//
//  Created by zhao on 2023/8/3.
//

#import "AppspModule.h"
#import "AppSpService.h"

@implementation AppspModule

#pragma mark - 同步方法

// 通过宏 UNI_EXPORT_METHOD_SYNC 将同步方法暴露给 js 端
UNI_EXPORT_METHOD_SYNC(@selector(initAppsp:))

/// 同步方法（注：同步方法会在 js 线程执行）
/// @param options js 端调用方法时传递的参数
/// 初始化
- (void)initAppsp:(NSDictionary *)options {
    NSLog(@"初始化入参：%@",options);
    NSString *appKey = options[@"appKey"];
    NSString *host = options[@"host"];
    BOOL debug = options[@"debug"];
    
    [[AppSpService shareService] initConfig:appKey withDebug:debug withHost:host];
}

#pragma mark - 异步方法

// 通过宏 UNI_EXPORT_METHOD 将异步方法暴露给 js 端
UNI_EXPORT_METHOD(@selector(getVersionModel:callback:))

/// 异步方法（注：异步方法会在主线程（UI线程）执行）
/// @param options js 端调用方法时传递的参数
/// @param callback 回调方法，回传参数给 js 端
/// 版本更新
- (void)getVersionModel:(NSDictionary *)options callback:(UniModuleKeepAliveCallback)callback {
    NSLog(@"版本更新入参：%@",options);
    
    [[AppSpService shareService] checkVersionUpdate:^(NSDictionary * _Nonnull successDic) {
        NSDictionary *result = [NSDictionary dictionary];
        if (successDic && successDic.count > 0 && !(successDic[@"repData"] == nil || [successDic[@"repData"] isEqual:[NSNull null]])) {
            result = @{
                @"code": @"success",
                @"downloadUrl": successDic[@"repData"][@"downloadUrl"],
                @"updateLog": successDic[@"repData"][@"updateLog"],
                @"showUpdate": successDic[@"repData"][@"showUpdate"],
                @"mustUpdate": successDic[@"repData"][@"mustUpdate"],
                @"msg": @"Get Appsp Info success"
            };
        } else {
            result = @{
                @"code": @"latest",
                @"msg": @"Empty data"
            };
        }
        if (callback) {
            callback(result, NO);
        }
    } withFailure:^(NSDictionary * _Nonnull failureDic) {
        NSDictionary *result = @{
            @"code": @"fail",
            @"msg": @"Parse error"
        };
        if (callback) {
            callback(result, NO);
        }
    }];
}

UNI_EXPORT_METHOD(@selector(getNoticeModel:callback:))

/// 获取公告信息
- (void)getNoticeModel:(NSDictionary *)options callback:(UniModuleKeepAliveCallback)callback {
    NSLog(@"公告信息入参：%@",options);
    
    [[AppSpService shareService] getNoticeInfo:^(NSDictionary * _Nonnull successDic) {
        if (callback) {
            callback(successDic, NO);
        }
    } withFailure:^(NSDictionary * _Nonnull failureDic) {
        if (callback) {
            callback(failureDic, NO);
        }
    }];
}

UNI_EXPORT_METHOD(@selector(openURL:callback:))

/// 跳转外部链接
- (void)openURL:(NSDictionary *)options callback:(UniModuleKeepAliveCallback)callback {
    NSLog(@"跳转外部链接入参：%@",options);
    NSString *urlString = options[@"url"];
    NSURL *url = [NSURL URLWithString:urlString];
    NSDictionary *urlOptions = @{UIApplicationOpenURLOptionUniversalLinksOnly: @NO};
    [[UIApplication sharedApplication] openURL:url options:urlOptions completionHandler:^(BOOL success) {
        if (callback) {
            callback([NSString stringWithFormat:@"%@", @(success)], NO);
        }
    }];
}

@end
